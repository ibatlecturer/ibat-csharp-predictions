﻿using System;
/*  Author: John Rowley
 *                                                C H A L L E N G E S
 *  Generate a Random Number
 *  Generate Lottery - 6 random numbers from pool of 50
 *  Generate Lottery - as above but no repeats
 *  Generate Lottery with a maxium of 6 picks - user must say how many picks
 *  Generate Lottery - as above but user can input their own choice for any number of picks and have random, max is 6
 *  All results must be sorted
 *  After each attempt, lottery must be triggered and you annouce how many they match and winner if 3 or more 
 *  Indicate how many wins they have (win 3, win 5, win 6)
 *  
 *  Reafactor all code into reusable functions 
 *  Catch Exceptions
 *                                              R E F E R E N C E S
 * General C Sharp: http://www.java2s.com/Tutorial/CSharp/0080__Statement/Dowhileloopwithaconsoleread.htm
 * General C Sharp: http://www.java2s.com/Tutorial/CSharp/0080__Statement/Dowhileloopwithaconsoleread.htm
 * Input Reading:  https://www.tutorialspoint.com/Way-to-read-input-from-console-in-Chash
 * Random Generation:  https://www.c-sharpcorner.com/article/generating-random-number-and-string-in-C-Sharp/
 * Array Sorting: https://www.geeksforgeeks.org/different-ways-to-sort-an-array-in-descending-order-in-c-sharp/
 * Do While input loop: http://www.java2s.com/Tutorial/CSharp/0080__Statement/Dowhileloopwithaconsoleread.htm
 */

namespace PredictionEngine
{
    class Program
    {
        static void Main(string[] args)
        {

            receiveInput();
            string[] predictions = new string[] {
                "It is certain.",
                "It is decidedly so.",
                "Without a doubt.",
                "Yes - definitely.",
                "You may rely on it.",
                "As I see it, yes.",
                "As I see it, yes.",
                "Most likely.",
                "Outlook good",
                "Yes.",
                "Signs point to yes.",
                "Reply hazy, try again",
                "Ask again later.",
                "Better not tell you now.",
                "Cannot predict now.",
                "Concentrate and ask again.",
                "Don't count on it.",
                "My reply is no",
                "My sources say no.",
                "Outlook not so good.",
                "Very doubtful."};


            Random random = new Random();
            int num = random.Next(predictions.Length - 1);
            Console.WriteLine("\t\t\t\t" + predictions[num]);
        }

        static public void receiveInput()
        {

            string val;
            Console.Write("Enter Integer: ");
            val = Console.ReadLine();

            int a = Convert.ToInt32(val);
            Console.WriteLine("Your input: {0}", a);


        }
    }
}
